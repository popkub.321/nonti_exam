import 'package:flutter/material.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:google_fonts/google_fonts.dart';

import '../DatabaseAPI/JSON_Convert.dart';
import '../DatabaseAPI/Nonti_Exam.dart';
import '../DatabaseAPI/remote_service.dart';
import '../Opject/detail.dart';
import '../Utils/colors.dart';

class Category_1 extends StatefulWidget {
  const Category_1({Key? key}) : super(key: key);

  @override
  State<Category_1> createState() => _Category_1State();
}

class _Category_1State extends State<Category_1> {
  late final String title;
  List<NotiEx>? notiExs;
  var isLoaded = false;

  /*NotiEx? _notiEx;
  getMethod() async{
    var url = Uri.parse(
        "https://msushop.comsciproject.net/MSUSHOP_API/index.php/get_Noti");
    var response = await http.get(url);
    var responsebody = json.decode(response.body);
    print(responsebody);
    print("Data id ${responsebody[0][0]}");
    //_notiEx = NotiEx.fromJson(json.decode(response.body));
   */ /* String IDEx = responsebody!.notiId.toString();
    String NameEx = responsebody!.name.toString();
    String Report = responsebody!.report.toString();
    print(IDEx);
    print(NameEx);
    print(Report);*/ /*
    return responsebody;
  }*/

  /*NotiEx? _notiEx;
  Future<void> NotiExample() async {
    var url = Uri.parse(
        "https://msushop.comsciproject.net/MSUSHOP_API/index.php/get_Noti");
    var response = await http.get(url);
    _notiEx = NotiEx.fromJson(json.decode(response.body));

    String NameEx = _notiEx!.name.toString();
    print(NameEx);

    if (response.statusCode == 200) {
      _notiEx = NotiEx.fromJson(json.decode(response.body));
      String IDEx = _notiEx!.notiId.toString();
     String NameEx = _notiEx!.name.toString();
     String Report = _notiEx!.report.toString();
      //print(IDEx);
      print(NameEx);
      print(Report);
      print('Request faile');
      setState(() {

       });

    } else {
      print('Request failed with status: ${response.statusCode}.');
    }
  }*/

  Future<void> _showNotification(String Exam1, String Exam2) async {
    const AndroidNotificationDetails androidNotificationDetails =
        AndroidNotificationDetails('channelId', 'channelName',
            importance: Importance.max,
            priority: Priority.high,
            ticker: 'ticker');

    const NotificationDetails platformChannelDetails = NotificationDetails(
      android: androidNotificationDetails,
    );

    await flutterLocalNotificationsPlugin.show(
        0, Exam1, Exam2, platformChannelDetails);
  }

  @override
  void initState() {
    super.initState();
    //NotiExample();
    getData();
  }

  getData() async {
    notiExs = await RemoteService().getnotiExs();
    if (notiExs != null) {
      setState(() {
        isLoaded = true;
      });
    }
  }

  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: DefaltColorApp,
        centerTitle: true,
        iconTheme: IconThemeData(color: Color(0xFFffffff),/*size: 30*/),
        title: Text(
          "Category1",
          style: GoogleFonts.squadaOne(color: Colors.white,fontSize: 25),

        ),
      ),

      body: Visibility(
        visible: isLoaded,
        child: ListView.builder(
          shrinkWrap: true,
          itemCount: notiExs?.length,
          itemBuilder: (context, index) {
            NotiEx notiex = notiExs![index];
            return Container(
              child: Card(
                shape: RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(10))),
                elevation: 1, //shadow card
                child: SizedBox(
                  child: InkWell(
                    //splashColor: Color(0xFFff4da6),
                    onLongPress: () {},
                    child: MaterialButton(
                      onPressed: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) =>
                                DetailNoti(notiex), //DetailPage
                          ),
                        );
                      },
                      child: Container(
                        child: Column(
                          children: [
                            Row(

                              /*children: [
                                        MaterialButton(
                                          minWidth: double.infinity,
                                          height: 60,
                                          onPressed: () {
                                            _showNotification(notiExs![index].report?? '',notiExs![index].name ?? '');

                                          },
                                          color: Colors.amberAccent,
                                          shape: RoundedRectangleBorder(

                                              side: BorderSide(color: Colors.black),
                                              borderRadius: BorderRadius.circular(30)),
                                          child: Text(
                                            "สร้างการแจ้งเตือน",
                                            style: TextStyle(
                                                fontWeight: FontWeight.w600, fontSize: 18),
                                          ),
                                        ),
                              ],*/
                            ),
                            Container(
                              padding:
                              const EdgeInsets.fromLTRB(
                                  0, 20, 0, 20),
                              child: Row(
                                children: <Widget>[
                                  Icon(
                                    Icons
                                        .notifications_active_outlined,
                                    size: 35,
                                    color: Colors.amber,
                                  ),
                                  /*Container(
                                                      height: 35,
                                                      width: 35,
                                                      decoration: BoxDecoration(
                                                          borderRadius:
                                                              BorderRadius
                                                                  .circular(12),
                                                          //color: Color(0xFFff4da6),
                                                          image: DecorationImage(
                                                            image: AssetImage(
                                                                "C:/nonti_exam/lib/assets/image/bell.png"),
                                                          )
                                                      ),
                                                    ),*/
                                  SizedBox(width: 16),
                                  Container(
                                    child: Expanded(
                                      child: Column(
                                        crossAxisAlignment:
                                        CrossAxisAlignment
                                            .start,
                                        children: [
                                          Text(
                                            notiExs![index]
                                                .name ??
                                                '',
                                            maxLines: 1,
                                            overflow:
                                            TextOverflow
                                                .ellipsis,
                                            style: GoogleFonts
                                                .lato(
                                              fontSize: 18,
                                              fontWeight:
                                              FontWeight
                                                  .bold,
                                            ),
                                          ),
                                          Text(
                                            notiExs![index]
                                                .report ??
                                                '',
                                            maxLines: 1,
                                            overflow:
                                            TextOverflow
                                                .ellipsis,
                                            style: GoogleFonts
                                                .lato(
                                              fontSize: 14,
                                              fontWeight:
                                              FontWeight
                                                  .normal,
                                              color: Colors.black54,
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                  /*IconButton(
                                                      onPressed: () {},
                                                      icon: Icon(
                                                        Icons.arrow_forward_ios,
                                                        size: 20,
                                                        color: Colors.black,
                                                      ),
                                                    ),*/
                                  Icon(
                                    Icons.arrow_forward_ios,
                                    size: 20,
                                    color: Colors.black,
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                ),
              ),
            );
          },
        ),
        replacement: const Center(
          child: CircularProgressIndicator(),
        ),
      ),

      /*FutureBuilder(
        //future: getMethod(),
        builder: (BuildContext context ,AsyncSnapshot snapshot){
          List snap = snapshot.data;
          if (snapshot.connectionState == ConnectionState.waiting){
            return Center(
              child: CircularProgressIndicator(),
            );
          }
          if (snapshot.hasError){
            return Center(
              child: Text("Error"),
            );
          }
          return ListView.builder(
              itemCount: snap.length,
              itemBuilder: (context ,index){
                return ListTile(
                //  title: Text(getMethod().notiId.toString()),
                  subtitle: Text("body ${snap[index]['']}"),
                );
              },

          );
          },
      )*/
      ///////////////////////////////////////////////////
      /*Container(
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [

              SizedBox(
                width: double.infinity,
                child: MaterialButton(
                  minWidth: double.infinity,
                  height: 60,
                  onPressed: () {

                    _showNotification();
                  },
                  color: Colors.orange,
                  shape: RoundedRectangleBorder(
                      side: BorderSide(color: Colors.white),
                      borderRadius: BorderRadius.circular(30)),
                  child: Text(
                    "ดูการแจ้งเตือน",
                    style: TextStyle(fontWeight: FontWeight.w600, fontSize: 18),
                  ),
                ),
              )
            ],
          ),
        ),
      ),*/
    );
  }
}
