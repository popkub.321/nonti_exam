import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class Help extends StatefulWidget {
  @override
  _HelpState createState() => _HelpState();
}

class _HelpState extends State<Help> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.greenAccent,
        title: Text(
          'Help List',
        ),
      ),
      body: Container(
        decoration: BoxDecoration(
          //ใส่พื้นหลังไล่สี
          gradient: LinearGradient(colors: [
            /*Color.fromARGB(100,200,200,1),
            Color.fromARGB(160, 148, 227, 1),*/
            Color(0x7EFF00F2),
            Color(0xFFFF85A1),
          ], begin: Alignment.topCenter, end: Alignment.bottomCenter),
        ),
        padding: const EdgeInsets.fromLTRB(0, 10, 0, 0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Icon(
              Icons.live_help,
              size: 100,
            ),
            Text(
              'Input Your Problem',
              textAlign: TextAlign.center,
              overflow: TextOverflow.ellipsis,
              style: TextStyle(
                fontSize: 20,
                color: Colors.black,
                fontWeight: FontWeight.bold,
              ),
            ),

            Padding(
              ////สร้างกรอบให้ textfield*********************
              padding: const EdgeInsets.all(8.0),
              child: TextField(
                decoration: InputDecoration(
                  hintText: 'Input Your Problem',
                  filled: true,
                  fillColor: Colors.white,
                ),
              ), //TextField1
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.fromLTRB(20, 10, 20, 10),
                  child: MaterialButton(
                    //shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(100)),//รูปทรงปุ่ม
                    color: Colors.black,
                    //textColor: Colors.white,
                    disabledColor: Colors.blueGrey, //สีปุ่ม
                    disabledTextColor: Colors.black, //สีตัวหนังสือ
                    splashColor: Colors.blueAccent,
                    onPressed: null,
                    child: Container(
                      child: Text(
                        'Submit',
                        style: TextStyle(fontSize: 20, color: Colors.black),
                      ),
                      padding: const EdgeInsets.all(13.0),
                    ),
                    padding: EdgeInsets.symmetric(vertical: 2, horizontal: 2),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.fromLTRB(20, 10, 20, 10),
                  //ตำแหน่งขอบปุ่ม
                  child: MaterialButton(
                    elevation: 10.0,
                    //ไฮต์ไลต์ เงาปุ่ม
                    highlightElevation: 20.0,
                    //ไฮต์ไลต์ เงาปุ่ม
                    color: Colors.black,
                    onPressed: () {
                      Navigator.pop(context);
                    },
                    //shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(100)), //รูปทรงปุ่ม
                    child: Container(
                      child: Text(
                        'Cancle',
                        style: TextStyle(fontSize: 20),
                      ),
                      decoration: BoxDecoration(
                        //ใส่พื้นหลังไล่สี
                        gradient: LinearGradient(
                            colors: [
                              /*Color.fromARGB(100,200,200,1),
            Color.fromARGB(160, 148, 227, 1),*/
                              Color(0xFFFF5D5D),
                              Color(0xFFECA4C0),
                            ],
                            begin: Alignment.topCenter,
                            end: Alignment
                                .bottomCenter), //ตัวกำหนดว่าจะไล่จากบนลงล่างหรือซ้ายไปขวา
                      ),
                      padding: const EdgeInsets.all(13.0), //ขนาดปุ่มภายใน
                    ),
                    padding: EdgeInsets.symmetric(vertical: 2, horizontal: 2), //ขอบขนาดปุ่มภายนอก //color: Colors.deepPurpleAccent,
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
