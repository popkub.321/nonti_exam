import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../Utils/colors.dart';

class Setting extends StatefulWidget {
  @override
  _SettingState createState() => _SettingState();
}

class _SettingState extends State<Setting> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: DefaltColorApp,
        title: Text(
          'Setting List',
        ),
      ),

      body: Container(
        /*decoration: BoxDecoration(
          //ใส่พื้นหลังไล่สี
          gradient: LinearGradient(colors: [
            /*Color.fromARGB(100,200,200,1),
            Color.fromARGB(160, 148, 227, 1),*/
            Color(0xFFF6BA47),
            Color(0xFFFF9292),
          ], begin: Alignment.topCenter, end: Alignment.bottomCenter),
        ),*/
        color: Colors.white,
        padding: const EdgeInsets.fromLTRB(20, 0, 20, 0),
        child: ListView(
          children: [
            SizedBox(height: 40),
            Row(
              children: [

                Icon(
                  Icons.person,
                  color: Colors.blue,
                ),
                SizedBox(width: 10),
                Text("Account",
                    style:
                        TextStyle(fontSize: 22, fontWeight: FontWeight.bold)),
              ],
            ),
            Divider(height: 20, thickness: 1), //สร้างเงาหรือกรอบIcon
            SizedBox(height: 10),
            buildAccountOption(context, "Change Password"),
            buildAccountOption(context, "Content Setting"),
            buildAccountOption(context, "Social"),
            buildAccountOption(context, "Language"),

            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.fromLTRB(20, 300, 20, 10),
                  child: MaterialButton(
                    //shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(100)),//รูปทรงปุ่ม
                    color: Colors.black,
                    //textColor: Colors.white,
                    disabledColor: Colors.blueGrey, //สีปุ่ม
                    disabledTextColor: Colors.black, //สีตัวหนังสือ
                    splashColor: Colors.blueAccent,
                    onPressed: null,
                    child: Container(
                      child: Text(
                        'Apply',
                        style: TextStyle(fontSize: 20, color: Colors.black),
                      ),
                      padding: const EdgeInsets.all(13.0),
                    ),
                    padding: EdgeInsets.symmetric(vertical: 2, horizontal: 2),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.fromLTRB(20, 300, 20, 10),
                  //ตำแหน่งขอบปุ่ม
                  child: MaterialButton(
                    elevation: 10.0,
                    //ไฮต์ไลต์ เงาปุ่ม
                    highlightElevation: 20.0,
                    //ไฮต์ไลต์ เงาปุ่ม
                    color: Colors.black,
                    onPressed: () {
                      Navigator.pop(context);
                    },
                    //////////////////////////////RaiseButton ขอบมน///////////////////////////////////
                    shape: StadiumBorder(), //รูปทรงปุ่ม
                    child: Container(
                      decoration: ShapeDecoration(
                        //ใส่พื้นหลังไล่สี
                        gradient: LinearGradient(
                            colors: [
                              /*Color.fromARGB(100,200,200,1),
            Color.fromARGB(160, 148, 227, 1),*/
                              Color(0xFFFF2E2E),
                              Color(0xFFECA4C0),
                            ],
                            begin: Alignment.topCenter,
                            end: Alignment
                                .bottomCenter),
                        shape: StadiumBorder(),//ตัวกำหนดว่าจะไล่จากบนลงล่างหรือซ้ายไปขวา
                      ),
                      child: Text(
                        'Cancle',
                        style: TextStyle(fontSize: 20),
                      ),
                      padding: const EdgeInsets.all(13.0), //ขนาดปุ่มภายใน
                    ),
                    padding: EdgeInsets.symmetric(vertical: 2, horizontal: 2), //ขอบขนาดปุ่มภายนอก //color: Colors.deepPurpleAccent,
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }

  GestureDetector buildAccountOption(BuildContext context, String title) {
    return GestureDetector(
      onTap: () {
        showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Text(title),
              content: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  Text("Option 1"),
                  Text("Option 2"),
                ],
              ),
              actions: [
                TextButton(
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                  child: Text("Close"),
                ),
              ],
            );
          },
        );
      },
      child: Padding(
        padding: const EdgeInsets.symmetric(vertical: 8, horizontal: 20),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(title,
                style: TextStyle(
                  fontSize: 20,
                  fontWeight: FontWeight.w500,
                  color: Colors.black87,
                )),
            Icon(Icons.arrow_forward_ios, color: Colors.black),
          ],
        ),
      ),
    );
  }
}
