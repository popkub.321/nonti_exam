import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

import '../Utils/colors.dart';


Card IconMenuHome(BuildContext context,   String PathLinkPage,IconData icons)=> Card (
  shape: const RoundedRectangleBorder(
      borderRadius: BorderRadius.all(Radius.circular(10))),
  color: DefaltColorApp,
  //elevation: 10,  //shadow
  child: SizedBox(
    height: 50,
    width: 50,
    child: Row(
      children: [
        IconButton(
          onPressed: () {
            Navigator.pushNamed(
                context, PathLinkPage);
          },
          icon: Icon(
            icons,
            size: 30,
            color: Colors.white,
          ),
        )
      ],
    ),
  ),
);

Padding TextIconMenuHome (String text)=> Padding (
  padding: const EdgeInsets.fromLTRB(0, 8, 0, 0),
  child: Text(
    text,
    style: GoogleFonts.lato(
      fontSize: 14,
      fontWeight: FontWeight.bold,
    ),
  ),
);



