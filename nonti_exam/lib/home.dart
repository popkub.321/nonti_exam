import 'package:backdrop/backdrop.dart';
import 'package:flutter/material.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:nonti_exam/Opject/detail.dart';
import 'package:nonti_exam/Utils/colors.dart';

import 'BackdropList/account.dart';
import 'BackdropList/help.dart';
import 'BackdropList/setting.dart';
import 'DatabaseAPI/JSON_Convert.dart';
import 'DatabaseAPI/Nonti_Exam.dart';
import 'DatabaseAPI/remote_service.dart';
import 'package:google_fonts/google_fonts.dart';

import 'UI/MenuIconButton.dart';

class HomePage extends StatefulWidget {
  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  late final String title;
  List<NotiEx>? notiExs;
  var isLoaded = false;

  /*NotiEx? _notiEx;
  getMethod() async{
    var url = Uri.parse(
        "https://msushop.comsciproject.net/MSUSHOP_API/index.php/get_Noti");
    var response = await http.get(url);
    var responsebody = json.decode(response.body);
    print(responsebody);
    print("Data id ${responsebody[0][0]}");
    //_notiEx = NotiEx.fromJson(json.decode(response.body));
   */ /* String IDEx = responsebody!.notiId.toString();
    String NameEx = responsebody!.name.toString();
    String Report = responsebody!.report.toString();
    print(IDEx);
    print(NameEx);
    print(Report);*/ /*
    return responsebody;
  }*/

  /*NotiEx? _notiEx;
  Future<void> NotiExample() async {
    var url = Uri.parse(
        "https://msushop.comsciproject.net/MSUSHOP_API/index.php/get_Noti");
    var response = await http.get(url);
    _notiEx = NotiEx.fromJson(json.decode(response.body));

    String NameEx = _notiEx!.name.toString();
    print(NameEx);

    if (response.statusCode == 200) {
      _notiEx = NotiEx.fromJson(json.decode(response.body));
      String IDEx = _notiEx!.notiId.toString();
     String NameEx = _notiEx!.name.toString();
     String Report = _notiEx!.report.toString();
      //print(IDEx);
      print(NameEx);
      print(Report);
      print('Request faile');
      setState(() {

       });

    } else {
      print('Request failed with status: ${response.statusCode}.');
    }
  }*/

  Future<void> _showNotification(String Exam1, String Exam2) async {
    const AndroidNotificationDetails androidNotificationDetails =
        AndroidNotificationDetails('channelId', 'channelName',
            importance: Importance.max,
            priority: Priority.high,
            ticker: 'ticker');

    const NotificationDetails platformChannelDetails = NotificationDetails(
      android: androidNotificationDetails,
    );

    await flutterLocalNotificationsPlugin.show(
        0, Exam1, Exam2, platformChannelDetails);
  }

  @override
  void initState() {
    super.initState();
    //NotiExample();
    getData();
  }

  getData() async {
    notiExs = await RemoteService().getnotiExs();
    if (notiExs != null) {
      setState(() {
        isLoaded = true;
      });
    }
  }

  Widget build(BuildContext context ) {
    return BackdropScaffold (
      //backgroundColor: Colors.deepOrange,
      //extendBodyBehindAppBar: true, เอาภาพ body คลุม appbar
      appBar: BackdropAppBar(
        title: Text(
          "R Service",
          style: GoogleFonts.squadaOne(color: Colors.white, fontSize: 30),
        ),
        iconTheme: IconThemeData(size: 30),
        /*actions: [
          BackdropToggleButton(icon: AnimatedIcons.list_view,)
        ],*/
        backgroundColor: DefaltColorApp,
        //backgroundColor: Colors.transparent,
        //backgroundColor: Colors.pinkAccent,
        /*leading: Card(
          child: Image(image: AssetImage(
              'C:/nonti_exam/lib/assets/image/backgrounddrawer.png'),fit: BoxFit.cover), //corner app bar image
        ),*/
        centerTitle: true,
      ),
      backLayer: Center(
        child: Expanded(
          child: ListView(
            children: [
              UserAccountsDrawerHeader(
                decoration: BoxDecoration(
                    color: DefaltColorApp,
                    image: DecorationImage(
                        image: AssetImage('assets/image/backgroundYellow.png'),
                        fit: BoxFit.cover)),
                currentAccountPicture: CircleAvatar(
                  backgroundColor: Colors.white,
                  /*child: Text(
                    "User",
                    style: GoogleFonts.pacifico(
                      color: DefaltColorApp,
                    ),
                  ),*/
                  backgroundImage: AssetImage('assets/image/R.png'),
                ),
                accountEmail: Text(
                  "Email",
                  style: GoogleFonts.lato(color: Colors.white),
                ),
                accountName: Text("Flutter Draw",
                    style: GoogleFonts.lato(
                      fontWeight: FontWeight.bold,
                      color: Colors.white,
                      fontSize: 20,
                    )),
              ),
              //ListTileFunction1
              ListTile(
                title: Text(
                  "Profile",
                  style: GoogleFonts.lato(),
                ),
                leading: Icon(Icons.account_box),
                onTap: () {
                  /*Navigator.push(context, new MaterialPageRoute(
                      builder: (context) => new ProfileList()),
                  ).then((value) => null);*/
                },
              ),
              ///////////////////////AccountList///////////////////////
              ListTile(
                title: Text('Account'),
                leading: Icon(Icons.padding),
                onTap: () {
                  Navigator.push(
                    context,
                    new MaterialPageRoute(builder: (context) => new Account()),
                  ).then((value) => null);
                },
              ),
              //////////////////////////SettingList////////////////////////////
              ListTile(
                title: Text('Setting'),
                leading: Icon(Icons.settings),
                onTap: () {
                  Navigator.push(
                    context,
                    new MaterialPageRoute(builder: (context) => new Setting()),
                  ).then((value) => null);
                },
              ),
              ////////////////////////////HelpList///////////////////////////
              /*ListTile(
                title: Text('Help'),
                leading: Icon(Icons.help),
                onTap: () {
                  Navigator.push(
                    context,
                    new MaterialPageRoute(builder: (context) => new Help()),
                  ).then((value) => null);
                },
              ),*/
              ListTile(
                title: Text(
                  "Log Out",
                  style: GoogleFonts.lato(),
                ),
                leading: Icon(Icons.close),
                onTap: () {
                  Navigator.popAndPushNamed(
                    context,
                    "/User_login",
                  );
                },
              ),
            ],
          ),
        ),
      ),

      frontLayer: Center(
        child: Expanded(
          child: Container(
            //color: Colors.black12,
            /*decoration: BoxDecoration(
              image: DecorationImage(
                  image: AssetImage(
                      'C:/nonti_exam/lib/assets/image/backgroundYellow.png'), //ใส่รูปพื้นหลัง
                  fit: BoxFit.cover),
            ),*/
            padding: const EdgeInsets.fromLTRB(0, 16, 0, 0),
            child: ListView(
              children: [
                Column(
                  children: [
                    SingleChildScrollView(
                      scrollDirection: Axis.horizontal,
                      child:
                        ////////////Card Category/////////////
                        Container(
                          //decoration: BoxDecoration(borderRadius: BorderRadius.all(Radius.circular(100))),
                          child: Column(
                            children: [
                              Row(
                                //mainAxisAlignment: MainAxisAlignment.spaceAround,
                                children: [
                                  Padding(
                                    padding: const EdgeInsets.all(8.0),
                                    child: Column(children: [
                                      IconMenuHome(context,"/Category_1",Icons.public),
                                      TextIconMenuHome("Publicize"),
                                    ]),
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.all(8.0),
                                    child: Column(children: [
                                      IconMenuHome(context,"/Category_2",Icons.browse_gallery),
                                      TextIconMenuHome("Activity"),
                                    ]),
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.all(8.0),
                                    child: Column(children: [
                                      IconMenuHome(context,"/Category_3",Icons.view_timeline),
                                      TextIconMenuHome("Document"),
                                    ]),
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.all(8.0),
                                    child: Column(children: [
                                      IconMenuHome(context,"/Category_4",Icons.dynamic_form),
                                      TextIconMenuHome("Other"),
                                    ]),
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.all(8.0),
                                    child: Column(children: [
                                      IconMenuHome(context,"/Category_4",Icons.dynamic_form),
                                      TextIconMenuHome("Other"),
                                    ]),
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.all(8.0),
                                    child: Column(children: [
                                      IconMenuHome(context,"/Category_4",Icons.dynamic_form),
                                      TextIconMenuHome("Other"),
                                    ]),
                                  ),

                                 /* Column(
                                    children: [
                                      Card(
                                        shape: RoundedRectangleBorder(
                                            borderRadius:
                                                BorderRadius.all(Radius.circular(10))),
                                        color: DefaltColorApp,
                                        //elevation: 10,  //shadow
                                        child: SizedBox(
                                          height: 50,
                                          width: 50,
                                          child: Row(
                                            children: [
                                              IconButton(
                                                onPressed: () {
                                                  Navigator.pushNamed(
                                                      context, "/Category_1");
                                                },
                                                icon: Icon(
                                                  Icons.public,
                                                  size: 30,
                                                  color: Colors.white,
                                                ),
                                              )
                                            ],
                                          ),
                                        ),
                                      ),
                                      Padding(
                                        padding: const EdgeInsets.fromLTRB(0, 8, 0, 0),
                                        child: Text(
                                          "Publicize",
                                          style: GoogleFonts.lato(
                                            fontSize: 14,
                                            fontWeight: FontWeight.bold,
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),*/
                                  /*Column(
                                    children: [
                                      Card(
                                        shape: RoundedRectangleBorder(
                                            borderRadius:
                                                BorderRadius.all(Radius.circular(10))),
                                        color: DefaltColorApp,
                                        //elevation: 10,  //shadow
                                        child: SizedBox(
                                          height: 50,
                                          width: 50,
                                          child: Row(
                                            children: [
                                              IconButton(
                                                onPressed: () {
                                                  Navigator.pushNamed(
                                                      context, "/Category_2");
                                                },
                                                icon: Icon(
                                                  Icons.browse_gallery,
                                                  size: 30,
                                                  color: Colors.white,
                                                ),
                                              )
                                            ],
                                          ),
                                        ),
                                      ),
                                      Padding(
                                        padding: const EdgeInsets.fromLTRB(0, 8, 0, 0),
                                        child: Text(
                                          "Activity",
                                          style: GoogleFonts.lato(
                                            fontSize: 14,
                                            fontWeight: FontWeight.bold,
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),*/
                                  /*Column(
                                    children: [
                                      Card(
                                        shape: RoundedRectangleBorder(
                                            borderRadius:
                                                BorderRadius.all(Radius.circular(10))),
                                        color: DefaltColorApp,
                                        //elevation: 10,  //shadow
                                        child: SizedBox(
                                          height: 50,
                                          width: 50,
                                          child: Row(
                                            children: [
                                              IconButton(
                                                onPressed: () {
                                                  Navigator.pushNamed(
                                                      context, "/Category_3");
                                                },
                                                icon: Icon(
                                                  Icons.view_timeline,
                                                  size: 30,
                                                  color: Colors.white,
                                                ),
                                              )
                                            ],
                                          ),
                                        ),
                                      ),
                                      Padding(
                                        padding: const EdgeInsets.fromLTRB(0, 8, 0, 0),
                                        child: Text(
                                          "Document",
                                          style: GoogleFonts.lato(
                                            fontSize: 14,
                                            fontWeight: FontWeight.bold,
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),*/
                                  /*Column(
                                    children: [
                                      Card(
                                        shape: RoundedRectangleBorder(
                                            borderRadius:
                                                BorderRadius.all(Radius.circular(10))),
                                        color: DefaltColorApp,
                                        //elevation: 10,  //shadow
                                        child: SizedBox(
                                          height: 50,
                                          width: 50,
                                          child: Row(
                                            children: [
                                              IconButton(
                                                onPressed: () {
                                                  Navigator.pushNamed(
                                                      context, "/Category_4");
                                                },
                                                icon: Icon(
                                                  Icons.dynamic_form,
                                                  size: 30,
                                                  color: Colors.white,
                                                ),
                                              )
                                            ],
                                          ),
                                        ),
                                      ),
                                      Padding(
                                        padding: const EdgeInsets.fromLTRB(0, 8, 0, 0),
                                        child: Text(
                                          "Other",
                                          style: GoogleFonts.lato(
                                            fontSize: 14,
                                            fontWeight: FontWeight.bold,
                                          ),
                                        ),
                                      ),
                                    ],
                                  )*/
                                ],
                              ),
                            ],
                          ),
                        ),
                       /* */
                        ///////////////ListView Notification////////////////
                      //

                    ),
                    const Divider(
                      thickness: 1,
                      indent: 15,
                      endIndent: 15,
                    ),
                    ShowDataNontification(),
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
      /*body: Visibility(
        visible: isLoaded,
        child: ListView.builder(
          itemCount: notiExs?.length,
          itemBuilder: (context, index) {
            return Container(
              child: Column(
                children: [Row(

                  /*children: [
                    MaterialButton(
                      minWidth: double.infinity,
                      height: 60,
                      onPressed: () {
                        _showNotification(notiExs![index].report?? '',notiExs![index].name ?? '');

                      },
                      color: Colors.amberAccent,
                      shape: RoundedRectangleBorder(

                          side: BorderSide(color: Colors.black),
                          borderRadius: BorderRadius.circular(30)),
                      child: Text(
                        "สร้างการแจ้งเตือน",
                        style: TextStyle(
                            fontWeight: FontWeight.w600, fontSize: 18),
                      ),
                    ),
                  ],*/
                ),
                  Container(
                    padding: const EdgeInsets.all(16),
                    child: Row(
                      children: <Widget>[

                        Container(
                          height: 50,
                          width: 50,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(12),
                            color: Colors.grey[300],
                          ),
                        ),
                        SizedBox(width: 16),
                        Expanded(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                notiExs![index].report?? '',
                                maxLines: 2,
                                overflow: TextOverflow.ellipsis,
                                style: const TextStyle(
                                  fontSize: 24,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                              Text(
                                notiExs![index].name ?? '',
                                maxLines: 3,
                                overflow: TextOverflow.ellipsis,
                              ),
                            ],
                          ),

                        ),


                      ],


                    ),



                  ),
                ],
              ),
            );
          },
        ),
      ),*/
      /*FutureBuilder(
        //future: getMethod(),
        builder: (BuildContext context ,AsyncSnapshot snapshot){
          List snap = snapshot.data;
          if (snapshot.connectionState == ConnectionState.waiting){
            return Center(
              child: CircularProgressIndicator(),
            );
          }
          if (snapshot.hasError){
            return Center(
              child: Text("Error"),
            );
          }
          return ListView.builder(
              itemCount: snap.length,
              itemBuilder: (context ,index){
                return ListTile(
                //  title: Text(getMethod().notiId.toString()),
                  subtitle: Text("body ${snap[index]['']}"),
                );
              },

          );
          },
      )*/
      ///////////////////////////////////////////////////
      /*Container(
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [

              SizedBox(
                width: double.infinity,
                child: MaterialButton(
                  minWidth: double.infinity,
                  height: 60,
                  onPressed: () {

                    _showNotification();
                  },
                  color: Colors.orange,
                  shape: RoundedRectangleBorder(
                      side: BorderSide(color: Colors.white),
                      borderRadius: BorderRadius.circular(30)),
                  child: Text(
                    "ดูการแจ้งเตือน",
                    style: TextStyle(fontWeight: FontWeight.w600, fontSize: 18),
                  ),
                ),
              )
            ],
          ),
        ),
      ),*/
    );
  }

  SingleChildScrollView ShowDataNontification() {
    return SingleChildScrollView(
                physics: ScrollPhysics(),
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Column(
                    children: [
                      Visibility(
                        visible: isLoaded,
                        child: ListView.builder(
                          shrinkWrap: true,
                          physics: NeverScrollableScrollPhysics(),
                          ///////ใส่แล้วข้อมูล List แสดง ถ้าไม่ใส่ข้อมูลไม่แสดง
                          itemCount: notiExs?.length,
                          itemBuilder: (context, index) {
                            NotiEx notiex = notiExs![index];
                            return Card(
                              shape: RoundedRectangleBorder(
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(10))),
                              elevation: 1, //shadow card
                              child: SizedBox(
                                child: InkWell(
                                  //splashColor: Color(0xFFff4da6),
                                  onLongPress: () {},
                                  child: MaterialButton(
                                    onPressed: () {
                                      Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                          builder: (context) =>
                                              DetailNoti (notiex), //DetailPage
                                        ),
                                      );
                                    },
                                    child: Container(
                                      child: Column(
                                        children: [
                                          Row(

                                              /*children: [
                                    MaterialButton(
                                      minWidth: double.infinity,
                                      height: 60,
                                      onPressed: () {
                                        _showNotification(notiExs![index].report?? '',notiExs![index].name ?? '');

                                      },
                                      color: Colors.amberAccent,
                                      shape: RoundedRectangleBorder(

                                          side: BorderSide(color: Colors.black),
                                          borderRadius: BorderRadius.circular(30)),
                                      child: Text(
                                        "สร้างการแจ้งเตือน",
                                        style: TextStyle(
                                            fontWeight: FontWeight.w600, fontSize: 18),
                                      ),
                                    ),
                          ],*/
                                              ),
                                          Container(
                                            padding:
                                                const EdgeInsets.fromLTRB(
                                                    0, 20, 0, 20),
                                            child: Row(
                                              children: <Widget>[
                                                Icon(
                                                  Icons
                                                      .notifications_active_outlined,
                                                  size: 35,
                                                  color: Colors.amber,
                                                ),
                                                /*Container(
                                                  height: 35,
                                                  width: 35,
                                                  decoration: BoxDecoration(
                                                      borderRadius:
                                                          BorderRadius
                                                              .circular(12),
                                                      //color: Color(0xFFff4da6),
                                                      image: DecorationImage(
                                                        image: AssetImage(
                                                            "C:/nonti_exam/lib/assets/image/bell.png"),
                                                      )
                                                  ),
                                                ),*/
                                                SizedBox(width: 16),
                                                Container(
                                                  child: Expanded(
                                                    child: Column(
                                                      crossAxisAlignment:
                                                          CrossAxisAlignment
                                                              .start,
                                                      children: [
                                                        Text(
                                                          notiExs![index]
                                                                  .name ??
                                                              '',
                                                          maxLines: 1,
                                                          overflow:
                                                              TextOverflow
                                                                  .ellipsis,
                                                          style: GoogleFonts
                                                              .lato(
                                                            fontSize: 18,
                                                            fontWeight:
                                                                FontWeight
                                                                    .bold,
                                                          ),
                                                        ),
                                                        Text(
                                                          notiExs![index]
                                                                  .report ??
                                                              '',
                                                          maxLines: 1,
                                                          overflow:
                                                              TextOverflow
                                                                  .ellipsis,
                                                          style: GoogleFonts
                                                              .lato(
                                                            fontSize: 14,
                                                            fontWeight:
                                                                FontWeight
                                                                    .normal,
                                                            color: Colors
                                                                .black54,
                                                          ),
                                                        ),
                                                      ],
                                                    ),
                                                  ),
                                                ),
                                                /*IconButton(
                                                  onPressed: () {},
                                                  icon: Icon(
                                                    Icons.arrow_forward_ios,
                                                    size: 20,
                                                    color: Colors.black,
                                                  ),
                                                ),*/
                                                Icon(
                                                  Icons.arrow_forward_ios,
                                                  size: 20,
                                                  color: Colors.black,
                                                ),
                                              ],
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                            );
                          },
                        ),
                      ),
                    ],
                  ),
                ),
              );
  }
}
