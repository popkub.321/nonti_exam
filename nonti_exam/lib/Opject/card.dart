import 'package:flutter/material.dart';

Card cardCategory (BuildContext context, Color colorscard, String LacatePage ,Icon cm,String namecard) =>
    Card(
      color: colorscard,
      elevation: 10,
      child: SizedBox(
        height: 50,
        width: 50,
        child: Column(
          children: [
            IconButton(
                onPressed: () {
                  Navigator.pushNamed(context,LacatePage);
                },
                icon: cm
            ),
            Text(namecard)
          ],
        ),
      ),
    );

