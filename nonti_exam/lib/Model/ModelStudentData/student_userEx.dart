import 'package:flutter/material.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:nonti_exam/DatabaseAPI/JSON_Convert.dart';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:nonti_exam/DatabaseAPI/JSON_Convert.dart';

import '../ModelLogIn/modelLoginConvert.dart';
import 'modelStudentUserConvert.dart';

final FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin =
FlutterLocalNotificationsPlugin();

class StudentData extends StatefulWidget {
  const StudentData({Key? key}) : super(key: key);

  @override
  State<StudentData> createState() => _StudentDataState();
}

class _StudentDataState extends State<StudentData> {
  ModelStudentData? _studentData;

  Future<void> StudentDataEx() async {
    var url = Uri.parse(
        "https://regpr.msu.ac.th/rservices/JsonStudent.php?studentcode=52011280022");
    var response = await http.get(url);

    if (response.statusCode == 200) {
      _studentData = ModelStudentData.fromJson(json.decode(response.body));
      String Studentcode = _studentData!.studentcode.toString();
      String Prefixname = _studentData!.prefixname.toString();
      String Studentname = _studentData!.studentname.toString();
      String Studentsurname = _studentData!.studentsurname.toString();
      String Facultyname = _studentData!.facultyname.toString();
      String Programname = _studentData!.programname.toString();
      String Levelname = _studentData!.levelname.toString();
      String Campusname = _studentData!.campusname.toString();

      setState(() {
        // _data = jsonResponse;
        // _Name = jsonResponse[0]["Name"];
      });
    } else {
      print('Request failed with status: ${response.statusCode}.');
    }
  }

  @override
  void initState() {
    super.initState();
    ModelLogIn();
  }

  Widget build(BuildContext context) {
    return Container();
  }
}
