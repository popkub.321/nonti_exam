import 'dart:ui';
import 'package:flutter/material.dart';
import 'package:nonti_exam/Utils/IconStyle.dart';

TextFormField TextformFieldEx1(String TextInField, IconData IconName) =>
    TextFormField(
      scrollPadding: EdgeInsets.all(10),
      decoration: InputDecoration(
        labelText: TextInField,
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(30),
        ),
        prefixIcon: Icon(IconName),
      ),
      //controller: usernameLoginSController,
    );

TextFormField TextformFieldEx2(String TextInField, IconData IconName) =>
    TextFormField(
      scrollPadding: EdgeInsets.all(10),
      decoration: InputDecoration(
        labelText: TextInField,
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(10),
        ),
        prefixIcon: Icon(IconName),
      ),
      //controller: usernameLoginSController,
    );

TextFormField FieldEX1(String Namefield, IconData IconTest) => TextFormField(
      scrollPadding: EdgeInsets.all(10),
      decoration: InputDecoration(
          labelText: Namefield,
          border: OutlineInputBorder(
            borderRadius: BorderRadius.circular(20),
          ),
          prefixIcon: IconEx3(IconTest)),
//controller: usernameLoginSController,
    );
