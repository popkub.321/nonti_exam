import 'package:http/http.dart' as http;
import 'package:nonti_exam/DatabaseAPI/JSON_Convert.dart';

import 'modelStudentUserConvert.dart';

class RemoteService {
  Future<List<ModelStudentData?>?> getstudentDatas() async {
    var client = http.Client();

    var uri = Uri.parse(
        "https://regpr.msu.ac.th/rservices/JsonStudent.php?studentcode=52011280022");
    var response = await client.get(uri);
    if (response.statusCode == 200) {
      var json = response.body;
      return modelStudentDataFromJson(json);
    }
  }
}
