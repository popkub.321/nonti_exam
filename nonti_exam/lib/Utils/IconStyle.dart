import 'dart:ui';
import 'package:flutter/material.dart';
import 'package:nonti_exam/Utils/StyleText.dart';
import 'package:nonti_exam/Utils/colors.dart';

Icon IconExam1(IconData IconName) =>
    Icon(IconName, size: 20, color: IconColorsBlack);

Icon IconExam2(IconData IconName) =>
    Icon(IconName, size: 20, color: IconColorsORNG);

Icon IconEx3(IconData Iconname) =>
    Icon(Iconname, size: 30, color: IconColorsBlack);
