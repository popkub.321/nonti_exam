import 'package:http/http.dart' as http;
import 'package:nonti_exam/DatabaseAPI/JSON_Convert.dart';

class RemoteService {
  Future<List<NotiEx>?> getnotiExs() async {
    var client = http.Client();

    var uri = Uri.parse(
        'https://msushop.comsciproject.net/MSUSHOP_API/index.php/get_Noti');
    var response = await client.get(uri);
    if (response.statusCode == 200) {
      var json = response.body;
      return notiExFromJson(json);
    }
    return null;
  }
}
