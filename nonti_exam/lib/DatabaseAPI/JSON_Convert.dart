import 'dart:convert';

import 'package:flutter/material.dart';

List<NotiEx> notiExFromJson(String str) =>
    List<NotiEx>.from(json.decode(str).map((x) => NotiEx.fromJson(x)));

String notiExToJson(List<NotiEx> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class NotiEx {
  NotiEx({
    @required this.notiId,
    @required this.name,
    @required this.report,
  });

  String? notiId;
  String? name;
  String? report;

  factory NotiEx.fromJson(Map<String, dynamic> json) => NotiEx(
        notiId: json["noti_ID"] == null ? null : json["noti_ID"],
        name: json["Name"] == null ? null : json["Name"],
        report: json["report"] == null ? null : json["report"],
      );

  Map<String, dynamic> toJson() => {
        "noti_ID": notiId == null ? null : notiId,
        "Name": name == null ? null : name,
        "report": report == null ? null : report,
      };
}
