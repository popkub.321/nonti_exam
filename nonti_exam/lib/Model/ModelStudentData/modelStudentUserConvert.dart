import 'dart:convert';

import 'package:flutter/cupertino.dart';

List<ModelStudentData?>? modelStudentDataFromJson(String str) => json.decode(str) == null
    ? []
    : List<ModelStudentData?>.from(
        json.decode(str)!.map((x) => ModelStudentData.fromJson(x)));

String modelStudentDataToJson(List<ModelStudentData?>? data) => json.encode(
    data == null ? [] : List<dynamic>.from(data!.map((x) => x!.toJson())));

class ModelStudentData {
  ModelStudentData({
    @required this.studentcode,
    @required this.prefixname,
    @required this.studentname,
    @required this.studentsurname,
    @required this.facultyname,
    @required this.programname,
    @required this.levelname,
    @required this.campusname,
  });

  String? studentcode;
  String? prefixname;
  String? studentname;
  String? studentsurname;
  String? facultyname;
  String? programname;
  String? levelname;
  String? campusname;

  factory ModelStudentData.fromJson(Map<String, dynamic> json) => ModelStudentData(
        studentcode: json["studentcode"],
        prefixname: json["prefixname"],
        studentname: json["studentname"],
        studentsurname: json["studentsurname"],
        facultyname: json["facultyname"],
        programname: json["programname"],
        levelname: json["levelname"],
        campusname: json["campusname"],
      );

  Map<String, dynamic> toJson() => {
        "studentcode": studentcode,
        "prefixname": prefixname,
        "studentname": studentname,
        "studentsurname": studentsurname,
        "facultyname": facultyname,
        "programname": programname,
        "levelname": levelname,
        "campusname": campusname,
      };
}
