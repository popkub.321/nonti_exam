import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:nonti_exam/Model/ModelStudentData/Remote_student_user.dart';
import 'package:nonti_exam/Utils/colors.dart';

import '../Model/ModelStudentData/modelStudentUserConvert.dart';

class Account extends StatefulWidget {
  @override
  _AccountState createState() => _AccountState();
}

class _AccountState extends State<Account> {
  late final String title;
  List<ModelStudentData>? studentDatas;
  var isLoaded = false;

  @override

  void initState() {
    super.initState();
    //NotiExample();
    getData();
  }

  getData() async {
    studentDatas =
        (await RemoteService().getstudentDatas())?.cast<ModelStudentData>();
    if (studentDatas != null) {
      setState(() {
        isLoaded = true;
      });
    }
  }

  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: DefaltColorApp,
        centerTitle: true,
        iconTheme: IconThemeData(
          color: Color(0xFFffffff),
        ),
        title: Text(
          'Account List',
          style: GoogleFonts.squadaOne(color: Colors.white, fontSize: 25),
        ),
        actions: <Widget>[
          IconButton(
            icon: const Icon(Icons.assignment_ind),
            tooltip: 'Go to the next page',
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute<void>(
                  builder: (BuildContext context) {
                    return Scaffold(
                      //Scaffold คือหน้าจอทั้งหมด
                      appBar: AppBar(
                        //title: Text(widget.title), //เปลี่ยนค่า title จาก widget title ที่กำหนดค่าไว้
                        title: Text(
                            'Flutter Demo By Boom'), //หรือเปลี่ยนได้เลย*********
                      ),
                      body: Container(
                        //color: Colors.greenAccent[200],
                        /*decoration: BoxDecoration(
                          //ใส่พื้นหลังไล่สี
                          gradient: LinearGradient(
                              colors: [
                                /*Color.fromARGB(100,200,200,1),
            Color.fromARGB(160, 148, 227, 1),*/
                                Color(0xFFDB80E0),
                                //Color(0xFFF8F8F8),
                                Color(0xFFF8CE57),
                              ],
                              begin: Alignment.topCenter,
                              end: Alignment.bottomCenter),
                        ),*/
                        color: Colors.white,
                      ), // This trailing comma makes auto-formatting nicer for build methods.
                    );
                  },
                ),
              );
            },
          )
        ],
      ),
      body: Container(
        /*decoration: BoxDecoration(
          //ใส่พื้นหลังไล่สี
          gradient: LinearGradient(colors: [
            /*Color.fromARGB(100,200,200,1),
            Color.fromARGB(160, 148, 227, 1),*/
            Color(0xFF8B40F5),
            Color(0xFF2DA3F3),
          ], begin: Alignment.topCenter, end: Alignment.bottomCenter),
        ),*/
        color: Colors.white,
        child: ListView.builder(
          itemBuilder: (context, index) {
            //ModelStudentData student = _studentDatas![index];
            child:
            return Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Icon(
                  Icons.account_circle_rounded,
                  size: 100,
                ),
                Text(
                  studentDatas![index].studentcode ?? '',
                  textAlign: TextAlign.start,
                  overflow: TextOverflow.ellipsis,
                  style: TextStyle(
                    fontSize: 20,
                    color: Colors.black,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                Text(
                  studentDatas![index].prefixname ?? '',
                  textAlign: TextAlign.start,
                  overflow: TextOverflow.ellipsis,
                  style: TextStyle(
                    fontSize: 20,
                    color: Colors.black,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                Text(
                  studentDatas![index].studentname ?? '',
                  textAlign: TextAlign.start,
                  overflow: TextOverflow.ellipsis,
                  style: TextStyle(
                    fontSize: 20,
                    color: Colors.black,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                Text(
                  studentDatas![index].studentsurname ?? '',
                  textAlign: TextAlign.start,
                  overflow: TextOverflow.ellipsis,
                  style: TextStyle(
                    fontSize: 20,
                    color: Colors.black,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                Text(
                  studentDatas![index].facultyname ?? '',
                  textAlign: TextAlign.start,
                  overflow: TextOverflow.ellipsis,
                  style: TextStyle(
                    fontSize: 20,
                    color: Colors.black,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                Text(
                  studentDatas![index].programname ?? '',
                  textAlign: TextAlign.start,
                  overflow: TextOverflow.ellipsis,
                  style: TextStyle(
                    fontSize: 20,
                    color: Colors.black,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                Text(
                  studentDatas![index].levelname ?? '',
                  textAlign: TextAlign.start,
                  overflow: TextOverflow.ellipsis,
                  style: TextStyle(
                    fontSize: 20,
                    color: Colors.black,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                Text(
                  studentDatas![index].campusname ?? '',
                  textAlign: TextAlign.start,
                  overflow: TextOverflow.ellipsis,
                  style: TextStyle(
                    fontSize: 20,
                    color: Colors.black,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ],
            );
          },
        ),
      ),
    );
  }
}
