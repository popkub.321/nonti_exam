import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:nonti_exam/DatabaseAPI/JSON_Convert.dart';
import 'package:nonti_exam/home.dart';

class DetailNoti extends StatelessWidget {
  final NotiEx notiex;

  const DetailNoti(this.notiex);

  @override
  Widget build(BuildContext context) {
    return Scaffold (
      appBar: AppBar(
        iconTheme: IconThemeData(color: Color(0xFFffffff),/*size: 30*/),
        backgroundColor: Color(0xFFFFC433), //0xFFff4da6
        title: Text(
          "Detail",
          style: GoogleFonts.squadaOne(color: Colors.white,fontSize: 25),
        ),
        centerTitle: true,
      ),
      body: SingleChildScrollView(
        physics: ScrollPhysics(),
        child: Container(
          child: Column(
            //mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Padding(
                padding: EdgeInsets.all(8.0),
                child: Text(
                  notiex.name.toString(),
                  style: GoogleFonts.squadaOne(
                    fontSize: 22,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
              Divider(thickness: 1,indent: 15,endIndent: 15,),
              Padding(
                padding: EdgeInsets.all(8.0),
                child: Text(
                  notiex.report.toString(),
                  style: GoogleFonts.squadaOne(
                    fontSize: 20,
                    fontWeight: FontWeight.normal,
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
