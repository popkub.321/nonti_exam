import 'package:http/http.dart' as http;
import 'package:nonti_exam/DatabaseAPI/JSON_Convert.dart';

import 'modelLoginConvert.dart';

class RemoteService {
  Future<List<ModelLogIn?>?> getmodelLogIn() async {
    var client = http.Client();

    var uri = Uri.parse(
        "https://regpr.msu.ac.th/rservices/JsonCheckAuthStudent.php?u=62011212075&p=62011212075boom");
    var response = await client.get(uri);
    if (response.statusCode == 200) {
      var json = response.body;
      return modelLogInFromJson(json);
    }
  }
}
