import 'dart:convert';

import 'package:flutter/material.dart';

List<ModelLogIn?>? modelLogInFromJson(String str) => json.decode(str) == null ? [] : List<ModelLogIn?>.from(json.decode(str)!.map((x) => ModelLogIn.fromJson(x)));

String modelLogInToJson(List<ModelLogIn?>? data) => json.encode(data == null ? [] : List<dynamic>.from(data!.map((x) => x!.toJson())));

class ModelLogIn {
  ModelLogIn({
    this.xpass,
  });

  String? xpass;

  factory ModelLogIn.fromJson(Map<String, dynamic> json) => ModelLogIn(
    xpass: json["xpass"],
  );

  Map<String, dynamic> toJson() => {
    "xpass": xpass,
  };
}
