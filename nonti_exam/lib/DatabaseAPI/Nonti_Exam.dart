import 'package:flutter/material.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:nonti_exam/DatabaseAPI/JSON_Convert.dart';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:nonti_exam/DatabaseAPI/JSON_Convert.dart';

final FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin =
    FlutterLocalNotificationsPlugin();

class NontiExam extends StatefulWidget {
  const NontiExam({Key? key}) : super(key: key);

  @override
  State<NontiExam> createState() => _NontiExamState();
}

class _NontiExamState extends State<NontiExam> {
  NotiEx? _notiEx;

  Future<void> NotiExample() async {
    var url = Uri.parse(
        "https://msushop.comsciproject.net/MSUSHOP_API/index.php/get_Noti");
    var response = await http.get(url);

    if (response.statusCode == 200) {
      _notiEx = NotiEx.fromJson(json.decode(response.body));
      String s = _notiEx!.name.toString();

      setState(() {
        // _data = jsonResponse;
        // _Name = jsonResponse[0]["Name"];
      });
    } else {
      print('Request failed with status: ${response.statusCode}.');
    }
  }

  @override
  void initState() {
    super.initState();
    NotiEx();
  }

  Widget build(BuildContext context) {
    return Container();
  }
}
