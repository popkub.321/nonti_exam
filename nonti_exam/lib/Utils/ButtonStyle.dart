import 'dart:ui';
import 'package:flutter/material.dart';
import 'package:nonti_exam/Utils/IconStyle.dart';
import 'package:nonti_exam/Utils/StyleText.dart';
import 'package:nonti_exam/Utils/colors.dart';

///////////////////  Button ////////////////////////////////////
MaterialButton ButtonExam1(
        BuildContext context, String TextInBT, String LacatePage) =>
    MaterialButton(
      minWidth: double.infinity,
      height: 60,
      onPressed: () {
        Navigator.pushNamed(context, LacatePage);
      },
      color: ButtonColorWH,
      shape: Border1,
      child: Text(TextInBT, style: StyleTextNew),
    );

MaterialButton ButtonExam2(
        BuildContext context, String TextInBT, String LacatePage) =>
    MaterialButton(
      minWidth: double.infinity,
      height: 60,
      onPressed: () {
        Navigator.pushNamed(context, LacatePage);
      },
      color: ButtonColorWH,
      shape: Border2,
      child: Text(TextInBT, style: StyleTextNew),
    );
////////////////////// Icons Buton Style//////////////////////////
IconButton IconButtonEx1(
        BuildContext context, String LocatePage, IconData NameIcon) =>
    IconButton(
      icon: IconExam1(NameIcon),
      onPressed: () {
        Navigator.pushNamed(context, LocatePage);
      },
    );

IconButton IconButtonEx2(
        BuildContext context, String LocatePage, IconData NameIcon) =>
    IconButton(
      icon: IconExam2(NameIcon),
      onPressed: () {
        Navigator.pushNamed(context, LocatePage);
      },
    );

/////////////////////////// NEW //////////////////////////////////////////

//////////////////////// RactangleStyle //////////////////////
RoundedRectangleBorder Border1 = RoundedRectangleBorder(
    side: BorderSide(color: ButtonColorBL),
    borderRadius: BorderRadius.circular(30));

RoundedRectangleBorder Border2 = RoundedRectangleBorder(
    side: BorderSide(color: ButtonColorBL),
    borderRadius: BorderRadius.circular(10));
