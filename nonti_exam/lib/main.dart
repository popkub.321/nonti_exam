import 'package:backdrop/backdrop.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:nonti_exam/DatabaseAPI/JSON_Convert.dart';
import 'package:nonti_exam/DatabaseAPI/remote_service.dart';

import 'DatabaseAPI/JSON_Convert.dart';
import 'DatabaseAPI/JSON_Convert.dart';
import 'Opject/detail.dart';
import 'category/category1.dart';
import 'category/category2.dart';
import 'category/category3.dart';
import 'category/category4.dart';
import 'home.dart';
import 'loginpage/User_Login.dart';

final FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin =
    FlutterLocalNotificationsPlugin();

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();

  /* AndroidInitializationSettings initializationSettingsAndroid =
  AndroidInitializationSettings("@mipmao/ic_launcher");*/

  var initializationSettingsAndroid =
      new AndroidInitializationSettings('@mipmap/ic_launcher');

  final InitializationSettings initializationSettings =
      InitializationSettings(android: initializationSettingsAndroid);

  await flutterLocalNotificationsPlugin.initialize(initializationSettings);

  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.amber,
        primaryColor: Colors.white10,
        //accentColor: Colors.black26
      ),
      home: const User_login(),
      initialRoute: '/',
      routes: {
        "/HomePage": (context) => HomePage(),
        "/User_login": (context) => User_login(),
        "/Category_1": (context) => Category_1(),
        "/Category_2": (context) => Category_2(),
        "/Category_3": (context) => Category_3(),
        "/Category_4": (context) => Category_4(),
        //"/DetailNoti": (context) => DetailNoti(),
      },
///?????
      
    );
  }
}

/*class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key, required this.title});

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  List<NotiEx>? notiExs;
  var isLoaded = false;



  /*NotiEx? _notiEx;
  getMethod() async{
    var url = Uri.parse(
        "https://msushop.comsciproject.net/MSUSHOP_API/index.php/get_Noti");
    var response = await http.get(url);
    var responsebody = json.decode(response.body);
    print(responsebody);
    print("Data id ${responsebody[0][0]}");
    //_notiEx = NotiEx.fromJson(json.decode(response.body));
   *//* String IDEx = responsebody!.notiId.toString();
    String NameEx = responsebody!.name.toString();
    String Report = responsebody!.report.toString();
    print(IDEx);
    print(NameEx);
    print(Report);*//*
    return responsebody;
  }*/



  /*NotiEx? _notiEx;
  Future<void> NotiExample() async {
    var url = Uri.parse(
        "https://msushop.comsciproject.net/MSUSHOP_API/index.php/get_Noti");
    var response = await http.get(url);
    _notiEx = NotiEx.fromJson(json.decode(response.body));

    String NameEx = _notiEx!.name.toString();
    print(NameEx);

    if (response.statusCode == 200) {
      _notiEx = NotiEx.fromJson(json.decode(response.body));
      String IDEx = _notiEx!.notiId.toString();
     String NameEx = _notiEx!.name.toString();
     String Report = _notiEx!.report.toString();
      //print(IDEx);
      print(NameEx);
      print(Report);
      print('Request faile');
      setState(() {

       });

    } else {
      print('Request failed with status: ${response.statusCode}.');
    }
  }*/


  Future<void> _showNotification( String Exam1 ,String Exam2) async {
    const AndroidNotificationDetails androidNotificationDetails =
        AndroidNotificationDetails('channelId', 'channelName',
            importance: Importance.max,
            priority: Priority.high,
            ticker: 'ticker');

    const NotificationDetails platformChannelDetails = NotificationDetails(
      android: androidNotificationDetails,
    );

    await flutterLocalNotificationsPlugin.show(
        0, Exam1, Exam2, platformChannelDetails);
  }



  @override
  void initState() {
    super.initState();
    //NotiExample();
    getData();

  }

  getData() async {
    notiExs = await RemoteService().getnotiExs();
    if (notiExs != null) {
      setState(() {
        isLoaded = true;
      });
    }
  }


  Widget build(BuildContext context) {
    return BackdropScaffold(
      appBar: AppBar(
        title: Text(widget.title),
        actions: [
          BackdropToggleButton(icon: AnimatedIcons.list_view,)
        ],
        backgroundColor: Colors.orangeAccent,
      ),
      backLayer: Center(
        child: ListView(
          children: [
            UserAccountsDrawerHeader(
              decoration: BoxDecoration(color: Colors.orangeAccent),
              currentAccountPicture: CircleAvatar(
                backgroundColor: Colors.white,
                child: Text("User"),
              ),
              accountEmail: Text("Email"),
              accountName: Text("Flutter Draw",style: TextStyle(fontSize: 20,)),
            ),
            //ListTileFunction1
            ListTile(
              title: Text("Profile"),
              leading: Icon(Icons.account_box),
              onTap: (){
                /*Navigator.push(context, new MaterialPageRoute(
                    builder: (context) => new ProfileList()),
                ).then((value) => null);*/
              },
            ),
            ListTile(
              title: Text("Log Out"),
              leading: Icon(Icons.close),
              onTap: (){Navigator.popAndPushNamed(context,"/Customer_login");},
            ),
          ],
        ),
      ),

      frontLayer: Container(
        padding: const EdgeInsets.all(16.0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            Card(
              color: Colors.orangeAccent,
              elevation: 10,
              child: SizedBox(
                height: 50,
                width: 50,
                child: Row(
                  children: [
                    IconButton(onPressed: (){
                      Navigator.pushNamed(context, "/Category_1");
                    }, icon: Icon(
                      Icons.account_box,
                      size: 20,
                      color: Colors.black,
                    ),)
                  ],
                ),
              ),
            ),
            Card(
              color: Colors.orangeAccent,
              elevation: 10,
              child: SizedBox(
                height: 50,
                width: 50,
                child: Row(
                  children: [
                    IconButton(onPressed: (){
                      Navigator.pushNamed(context, "/Category_2");
                    }, icon: Icon(
                      Icons.account_balance,
                      size: 20,
                      color: Colors.black,
                    ),)
                  ],
                ),
              ),
            ),
            Card(
              color: Colors.orangeAccent,
              elevation: 10,
              child: SizedBox(
                height: 50,
                width: 50,
                child: Row(
                  children: [
                    IconButton(onPressed: (){
                      Navigator.pushNamed(context, "/Category_3");
                    }, icon: Icon(
                      Icons.abc,
                      size: 20,
                      color: Colors.black,
                    ),)
                  ],
                ),
              ),
            ),
            Card(
              color: Colors.orangeAccent,
              elevation: 10,
              child: SizedBox(
                height: 50,
                width: 50,
                child: Row(
                  children: [
                    IconButton(onPressed: (){
                      Navigator.pushNamed(context, "/Category_4");
                    }, icon: Icon(
                      Icons.ac_unit_outlined,
                      size: 20,
                      color: Colors.black,
                    ),)
                  ],
                ),
              ),
            )
          ],
        ),
      ),
      /*body: Visibility(
        visible: isLoaded,
        child: ListView.builder(
          itemCount: notiExs?.length,
          itemBuilder: (context, index) {
            return Container(
              child: Column(
                children: [Row(

                  /*children: [
                    MaterialButton(
                      minWidth: double.infinity,
                      height: 60,
                      onPressed: () {
                        _showNotification(notiExs![index].report?? '',notiExs![index].name ?? '');

                      },
                      color: Colors.amberAccent,
                      shape: RoundedRectangleBorder(

                          side: BorderSide(color: Colors.black),
                          borderRadius: BorderRadius.circular(30)),
                      child: Text(
                        "สร้างการแจ้งเตือน",
                        style: TextStyle(
                            fontWeight: FontWeight.w600, fontSize: 18),
                      ),
                    ),
                  ],*/
                ),
                  Container(
                    padding: const EdgeInsets.all(16),
                    child: Row(
                      children: <Widget>[

                        Container(
                          height: 50,
                          width: 50,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(12),
                            color: Colors.grey[300],
                          ),
                        ),
                        SizedBox(width: 16),
                        Expanded(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                notiExs![index].report?? '',
                                maxLines: 2,
                                overflow: TextOverflow.ellipsis,
                                style: const TextStyle(
                                  fontSize: 24,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                              Text(
                                notiExs![index].name ?? '',
                                maxLines: 3,
                                overflow: TextOverflow.ellipsis,
                              ),
                            ],
                          ),

                        ),


                      ],


                    ),



                  ),
                ],
              ),
            );
          },
        ),
      ),*/
      /*FutureBuilder(
        //future: getMethod(),
        builder: (BuildContext context ,AsyncSnapshot snapshot){
          List snap = snapshot.data;
          if (snapshot.connectionState == ConnectionState.waiting){
            return Center(
              child: CircularProgressIndicator(),
            );
          }
          if (snapshot.hasError){
            return Center(
              child: Text("Error"),
            );
          }
          return ListView.builder(
              itemCount: snap.length,
              itemBuilder: (context ,index){
                return ListTile(
                //  title: Text(getMethod().notiId.toString()),
                  subtitle: Text("body ${snap[index]['']}"),
                );
              },

          );
          },
      )*/
      ///////////////////////////////////////////////////
      /*Container(
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [

              SizedBox(
                width: double.infinity,
                child: MaterialButton(
                  minWidth: double.infinity,
                  height: 60,
                  onPressed: () {

                    _showNotification();
                  },
                  color: Colors.orange,
                  shape: RoundedRectangleBorder(
                      side: BorderSide(color: Colors.white),
                      borderRadius: BorderRadius.circular(30)),
                  child: Text(
                    "ดูการแจ้งเตือน",
                    style: TextStyle(fontWeight: FontWeight.w600, fontSize: 18),
                  ),
                ),
              )
            ],
          ),
        ),
      ),*/
    );
  }
}
*/
