import 'package:json_annotation/json_annotation.dart';
import 'package:retrofit/retrofit.dart';
import 'package:dio/dio.dart';

import '../DatabaseAPI/JSON_Convert.dart';

part 'api_helper.g.dart';

@RestApi(baseUrl: "https://msushop.comsciproject.net/MSUSHOP_API/index.php/")
abstract class RestClient {
  factory RestClient(Dio dio) = _RestClient;
  @GET("/get_Noti")
  Future<List<NotiEx>> getNontiData();
}
/*

@JsonSerializable()
class NotiEx{
  int index;
  String name;
  String picture;
  String gender;
  int age;
  String email;
  String phone;
  String company;

  NotiEx({
    required this.index,
    required this.name,
    required this.picture,
    required this.gender,
    required this.age,
    required this.email,
    required this.phone,
    required this.company});

  factory NotiEx.fromJson(Map<String, dynamic> json) => _$PostFromJson(json);
  Map<String, dynamic> toJson() => _$PostToJson(this);
}*/
