import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:nonti_exam/utils/colors.dart';

import '../main.dart';

class User_login extends StatefulWidget {
  const User_login({Key? key}) : super(key: key);

  @override
  State<User_login> createState() => _User_loginState();
}

class _User_loginState extends State<User_login> {
  final u = TextEditingController();
  final p = TextEditingController();

  String? usernameLoginC;
  String? passwordLoginC;

  void _setText() {
    setState(() {
      usernameLoginC = u.text;
      passwordLoginC = p.text;
    });
  }

  bool _isHidden = true;
  bool isObsecure = true;

  //final Color facebookColor = const Color(0xff39579A);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      backgroundColor: Colors.white,
      appBar: AppBar(
        elevation: 0,
        brightness: Brightness.light,
        backgroundColor: Colors.white,
      ),
      body: Container(
        height: MediaQuery.of(context).size.height,
        width: double.infinity,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Expanded(
                child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                SizedBox(height: 50),
                Container(
                  child: Center(
                      child: Container(
                          height: 120,
                          width: 120,
                          child: Image.asset('assets/image/R.png'))),
                ),
                SizedBox(
                  height: 20,
                ),
                Column(
                  children: <Widget>[
                    Text(
                      "R Service",
                      style:
                          TextStyle(fontSize: 30, fontWeight: FontWeight.bold),
                    ),
                     SizedBox(height: 10,),
                    Text(
                      "กรุณากรอกข้อมูลเพื่อเข้าสู่ระบบ",
                      style: TextStyle(fontSize: 15, color: Colors.grey[700]),
                    )
                  ],
                ),
                SizedBox(
                  height: 20,
                ),
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: 40),
                  child: Column(
                    children: <Widget>[
                      TextFormField(
                        scrollPadding: EdgeInsets.all(10),
                        decoration: InputDecoration(
                          labelText: 'ชื่อผู้ใช้',
                          border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(30),
                          ),
                          prefixIcon: Icon(Icons.perm_identity_outlined),
                        ),
                        controller: u,
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      TextFormField(
                        scrollPadding: EdgeInsets.all(10),
                        obscureText: isObsecure,
                        decoration: InputDecoration(
                          labelText: 'รหัสผ่าน',
                          suffixIcon: IconButton(
                            onPressed: () {
                              setState(() {
                                isObsecure = !isObsecure;
                              });
                            },
                            icon: Icon(
                              isObsecure
                                  ? Icons.visibility_off
                                  : Icons.visibility,
                            ),
                          ),
                          border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(30),
                          ),
                          prefixIcon: Icon(Icons.vpn_key_sharp),
                        ),
                        controller: p,
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      Column(
                        children: <Widget>[
                          MaterialButton(
                            minWidth: double.infinity,
                            height: 60,
                            onPressed: () {
                              Navigator.pushNamed(context, '/HomePage');
                              print("Username is :  " +
                                  u.text);
                              print("Password is :  " +
                                  p.text);
                            },
                            color: Color(0xFFFFC433),
                            shape: RoundedRectangleBorder(
                                side: BorderSide(color: Colors.grey),
                                borderRadius: BorderRadius.circular(30)),
                            child: Text(
                              "เข้าสู่ระบบ",
                              style: TextStyle(
                                  color: Colors.white,
                                  fontWeight: FontWeight.w600,
                                  fontSize: 18),
                            ),
                          ),
                          SizedBox(
                            height: 20,
                          ),
                          //_textAccount(),
                          SizedBox(
                            height: 10,
                          ),
                          /*_LoginWithFacebook(),

                              CustomWidgets.socialButtonRect(
                                  'เข้าสู่ระบบด้วย Facebook', facebookColor, FontAwesomeIcons.facebookF,
                                  onTap: () {
                                    Navigator.pushNamed(context, "/Welcome_page");
                                    //Fluttertoast.showToast(msg: 'I am facebook');
                                  }),*/
                        ],
                      )
                    ],
                  ),
                ),
              ],
            ))
          ],
        ),
      ),
    );
  }

  void _togglePasswordView() {
    setState(() {
      _isHidden = !_isHidden;
    });
  }

/*RichText _textAccount() {
    return RichText(
      text: TextSpan(
          text: "หากไม่มีบัญชีผู้ใช้  ",
          children: [
            TextSpan(
              style: TextStyle(color: Colors.deepOrange),
              text: 'สร้างบัญชีผู้ใช้ใหม่',
              recognizer: TapGestureRecognizer()
                ..onTap = () => Navigator.pushNamed(context, "/Sign_Customer"),
            )
          ],
          style: TextStyle(
              color: Colors.black87, fontSize: 14, fontFamily: 'Exo2')),
    );
  }

  RichText _LoginWithFacebook() {
    return RichText(
      text: TextSpan(
          text: "หรือเข้าสู่ระบบโดยใช้ ",
          children: [
            TextSpan(
              style: TextStyle(color: Colors.blue),
              text: 'Facebook',
              recognizer: TapGestureRecognizer()
                ..onTap = () => Navigator.pushNamed(context, "/Welcome_page"),
            )
          ],
          style: TextStyle(
              color: Colors.black87, fontSize: 14, fontFamily: 'Exo2')),
    );
  }*/
}
